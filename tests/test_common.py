import unittest
import inspect
import os.path as op

import numpy as np
from scipy.io import loadmat


MAX_PERFORMANCE_RATIO = 4  # Maximum performance ratio against matplab


class LazyMatlabEngine(object):
    def __init__(self, script_dir=None):
        self.script_dir = script_dir
        self.engine = None

    def stop(self):
        if self.engine is not None:
            self.engine.quit()

    def update_ref(self, script_name, force=False):
        m_path = op.join(self.script_dir, script_name + '.m')
        mat_path = op.join(self.script_dir, script_name + '.mat')

        # Check if mat file requires update
        if not op.exists(mat_path) or \
           op.getmtime(m_path) > op.getmtime(mat_path):
            # Launching engine if needed
            if self.engine is None:
                import matlab.engine
                self.engine = matlab.engine.start_matlab()
            # Adding script_dir to matlab path list
            self.engine.cd(self.script_dir, nargout=0)
            # Calling matlab script
            print('Updating %s' % mat_path)
            getattr(self.engine, script_name)(nargout=0)

    def load_ref(self, script_name, f2c=False):
        """
        Loads reference variables from Matlab .mat file.

        Args:
            filename(string): .mat filename.
        """
        # Updating ref if needed
        self.update_ref(script_name)

        # Loading matlab .mat file
        refs = loadmat(op.join(self.script_dir, script_name + '.mat'),
                       squeeze_me=True)

        # Changing f-arrays to c-arrays
        for k in refs:
            if isinstance(refs[k], np.ndarray):
                refs[k] = refs[k].T

        return refs


def extract_args_and_call(callable, references, constructor=False):
    """
    Extracts arguments lists from callable and calls it with arguments
    from references dictionary.

    Args:
        callable(object): Callable object.
        references(dict): References dictionary.

    Returns:
        Callable result.
    """
    # Determining arguments list
    arg_names = inspect.getargspec(
        callable.__init__ if constructor else callable).args

    # Filtering keys from references
    kwargs = {k: v for k, v in references.items() if k in arg_names}

    # Calling callable with arguments
    return callable(**kwargs)


class MatlabTester(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        dir_path = op.dirname(op.realpath(__file__))
        cls.matlab_engine = LazyMatlabEngine(op.join(dir_path, 'matlab'))

    @classmethod
    def tearDownClass(cls):
        cls.matlab_engine.stop()