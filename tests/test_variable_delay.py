# -*- coding: utf-8 -*-
from builtins import range
import time

import numpy as np
from numpy.testing import assert_array_almost_equal

from test_common import (MatlabTester, extract_args_and_call,
                         MAX_PERFORMANCE_RATIO)
import dsp.variable_delay as vd


class VariableIntegerDelayTester(MatlabTester):

    def test_roll_buffer(self):
        initials = np.array([0, 0, 0, 0, 1, 2])
        sample = np.array([3, 4, 5])

        buffer = vd.construct_roll_buffer(sample, initials, max_delay=6)

        ref_buffer = np.array([[3, 4, 5],
                               [2, 3, 4],
                               [1, 2, 3],
                               [0, 1, 2],
                               [0, 0, 1],
                               [0, 0, 0],
                               [0, 0, 0]])

        assert_array_almost_equal(ref_buffer, buffer)

    def test_sequential(self):
        refs = self.matlab_engine.load_ref(
            'test_variable_integer_delay_sequential', f2c=True)
        refs['max_delay'] = 20
        dobj = vd.VariableIntegerDelay()

        y = np.zeros(refs['x'].shape)
        for k in range(10):
            x_temp = refs['x'][k*10: (k + 1)*10]
            delay = k*np.ones(10)
            y_temp = dobj.step(x_temp, delay)
            y[k*10: (k + 1)*10] = y_temp

        assert_array_almost_equal(y, refs['y'])

    def test_single(self):
        refs = self.matlab_engine.load_ref(
            'test_variable_integer_delay_single', f2c=True)

        dobj = extract_args_and_call(vd.VariableIntegerDelay, refs,
                                     constructor=True)
        y = extract_args_and_call(dobj.step, refs)

        assert_array_almost_equal(y, refs['y'])

    def test_initials(self):
        refs = self.matlab_engine.load_ref(
            'test_variable_integer_delay_initials', f2c=True)

        dobj = extract_args_and_call(vd.VariableIntegerDelay, refs,
                                     constructor=True)
        y = extract_args_and_call(dobj.step, refs)

        assert_array_almost_equal(y, refs['y'])

    def test_initials_const(self):
        refs = self.matlab_engine.load_ref(
            'test_variable_integer_delay_const_initials', f2c=True)

        dobj = extract_args_and_call(vd.VariableIntegerDelay, refs,
                                     constructor=True)
        y = extract_args_and_call(dobj.step, refs)

        assert_array_almost_equal(y, refs['y'])

    def test_multi_channel(self):
        refs = self.matlab_engine.load_ref(
            'test_variable_integer_delay_multi_channel', f2c=True)

        dobj = extract_args_and_call(vd.VariableIntegerDelay, refs,
                                     constructor=True)
        y = extract_args_and_call(dobj.step, refs)

        assert_array_almost_equal(y, refs['y'])

    def test_performance(self):
        refs = self.matlab_engine.load_ref(
            'test_variable_integer_delay_perf', f2c=True)
        start = time.time()

        dobj = extract_args_and_call(vd.VariableIntegerDelay, refs,
                                     constructor=True)
        y = extract_args_and_call(dobj.step, refs)

        elapsed = time.time() - start
        assert_array_almost_equal(y, refs['y'])
        self.assertLess(elapsed, MAX_PERFORMANCE_RATIO * refs['elapsed'],
                        'Test slower than Matlab')


class VariableFractionalDelayTester(MatlabTester):

    def test_single(self):
        refs = self.matlab_engine.load_ref(
            'test_variable_fractional_delay_single', f2c=True)
        dobj = extract_args_and_call(vd.VariableFractionalDelay, refs,
                                     constructor=True)
        y = extract_args_and_call(dobj.step, refs)

        assert_array_almost_equal(y, refs['y'])

    def test_const(self):
        refs = self.matlab_engine.load_ref(
            'test_variable_fractional_delay_const', f2c=True)
        dobj = extract_args_and_call(vd.VariableFractionalDelay, refs,
                                     constructor=True)
        y = extract_args_and_call(dobj.step, refs)

        assert_array_almost_equal(y, refs['y'])

    def test_complex(self):
        refs = self.matlab_engine.load_ref(
            'test_variable_fractional_delay_complex', f2c=True)
        dobj = extract_args_and_call(vd.VariableFractionalDelay, refs,
                                     constructor=True)
        y = extract_args_and_call(dobj.step, refs)

        assert_array_almost_equal(y, refs['y'])

    def test_performance(self):
        refs = self.matlab_engine.load_ref(
            'test_variable_fractional_delay_perf', f2c=True)
        start = time.time()
        dobj = extract_args_and_call(vd.VariableFractionalDelay, refs,
                                     constructor=True)
        y = extract_args_and_call(dobj.step, refs)

        elapsed = time.time() - start
        assert_array_almost_equal(y, refs['y'])
        self.assertLess(elapsed, MAX_PERFORMANCE_RATIO * refs['elapsed'],
                        'Test slower than Matlab')
