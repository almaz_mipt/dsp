clear all;
close all;

h = dsp.VariableIntegerDelay();
x = [1:20; 21:40]';
%delay = [mod(1: 20, 5) * 2; mod(1: 20, 5) * 2]';
delay = [3, 4];
y = step(h, x, delay);

for i=1:2
    figure;
    stem(x(:, i),'b'); hold on;
    stem(y(:, i), 'r');
    legend('Original Signal', 'Variable Integer Delayed Signal')
end

save('test_variable_integer_delay_multi_channel.mat')