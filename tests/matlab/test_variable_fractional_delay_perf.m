clear all;
close all;

h = dsp.VariableFractionalDelay;

tic;

sample_size = 3000;
num_channels = 500;

x = repmat((1:sample_size)', 1, num_channels);
delay = repmat((mod(1: sample_size, 5) / 10)', 1, num_channels);
y = step(h, x, delay);


elapsed = toc;
disp(elapsed)

save('test_variable_fractional_delay_perf.mat')