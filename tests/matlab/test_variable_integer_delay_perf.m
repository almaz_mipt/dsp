clear all;
close all;

h = dsp.VariableIntegerDelay;

tic;

sample_size = 3000;
num_channels = 500;

x = repmat((1:sample_size)', 1, num_channels);
delay = repmat((mod(1: sample_size, 5)*2)', 1, num_channels);
y = step(h, x, delay);


elapsed = toc;
disp(elapsed)


for i=1:2
    figure;
    stem(x(:, i),'b'); hold on;
    stem(y(:, i), 'r');
    legend('Original Signal', 'Variable Integer Delayed Signal')
end

save('test_variable_integer_delay_perf.mat')