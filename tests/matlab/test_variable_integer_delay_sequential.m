clear all;
close all;

h = dsp.VariableIntegerDelay;

x = 1:100;
y = [];

for k = 0:9
    delay = k*ones(10, 1);
    x_temp = x(k*10 + 1:(k+1)*10)';
    y_step = step(h, x_temp, delay);
    y = [y; y_step];   
end

stem(x,'b');
hold on; stem(y,'r');
legend('Original Signal', 'Variable Integer Delayed Signal')

save('test_variable_integer_delay_sequential.mat')