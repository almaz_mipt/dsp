clear all;
close all;

h = dsp.VariableFractionalDelay;

x = 1:100;
delay = mod(1: 100, 5) / 10;
y = step(h, x', delay');

stem(x,'b');
hold on; stem(y, 'r');
legend('Original Signal', 'Variable Integer Delayed Signal')

save('test_variable_fractional_delay_single.mat')