clear all;
close all;

hsr = dsp.SignalSource;
hvfd = dsp.VariableFractionalDelay;
hLog = dsp.SignalSink;

for ii = 1:10
    delayedsig = hvfd(hsr(), ii/10);
    hLog(delayedsig);
end

sigd = hLog.Buffer;

stem(hsr.Signal, 1:10, 'b')
hold on;
stem(sigd.', 1:10, 'r');
legend('Original signal',...
    'Variable fractional delayed signal', ...
    'Location','best')

save('test_variable_fractional_delay.mat')