clear all;
close all;

x = 1:100;
delay = mod(1: 100, 5) * 2;
initials = -10;
max_delay = 12;

h = dsp.VariableIntegerDelay('InitialConditions', initials,...
                             'MaximumDelay', max_delay);
y = step(h, x', delay');

stem(x,'b');
hold on; stem(y, 'r');
legend('Original Signal', 'Variable Integer Delayed Signal')

save('test_variable_integer_delay_const_initials.mat')
