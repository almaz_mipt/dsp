clear all;
close all;

h = dsp.VariableFractionalDelay;

x = (1:100)';
delay = 0.3;
y = step(h, x, delay);

stem(x,'b');
hold on; stem(y, 'r');
legend('Original Signal', 'Variable Integer Delayed Signal')

save('test_variable_fractional_delay_const.mat')