# -*- coding: utf-8 -*-
from builtins import range

import numpy as np


def construct_roll_buffer(sample, initials, max_delay=100):
    """
    Constructs special buffer with shape
    (max_delay + 1, sample_len), where each column represents
    signal shifted for number of samples equal to the column number.
    The function takes into account given initial values.

    Args:
        signal(array): Input signal with shape (sample_len, channels_num)
        initials(array): Initial conditions with shape
        (max_delay, channels_num).
        max_size(int): Maximum delay samples memory.
    Returns:
        Buffer.
    """
    # Number of channels
    sample_len = len(sample)

    # Allocating memory
    buffer = np.zeros((max_delay + 1, sample_len), dtype=sample.dtype)

    # Initials + sample
    total = np.hstack((initials, sample))

    # Filling buffer
    for i in range(max_delay + 1):
        buffer[i] = total[len(initials)-i:len(total)-i]
    return buffer


class VariableIntegerDelay(object):
    """
    Delays input by time-varying integer number of sample periods.

    Attributes:
        max_delay (int): The maximum computable delay. Any input delay values
            greater than max_delay are clipped.
        initials (float or array_like): Initial conditions for the signal.
        Default is zeros. Can be constant value, array with constant values
        per channel with shape (self.max_delay, ) or array with variable
        values with shape (num_channels, max_delay).

    """

    def __init__(self, max_delay=100, initials=None):
        """
        Init object with max_delay and initials.
        """
        # Maximum delay value validation
        if not np.isscalar(max_delay) or max_delay <= 0:
            raise ValueError('Maximum delay must be positive integer number.')
        else:
            self.max_delay = int(max_delay)

        # Initial conditions validation
        self.initials = None
        if initials is not None:
            if np.ndim(initials) > 2:
               ValueError('Invalid initials dimension')
            else:
                self.initials = initials

    def reset(self):
        """
        Resets internal initial conditions.
        """
        self.initials = None

    def step_input_validation(self, x, delay):
        # Input sample channels validation
        if not np.isscalar(x):
            # Checking for integer input
            if np.issubdtype(x.dtype, np.integer):
                x = x.astype(np.float_, copy=False)
            # Single channel vector (sample_len, )
            if np.ndim(x) == 1:
                x = np.asarray(x)[np.newaxis, :]
            # Multiple channel vector (num_channels, sample_len)
            elif np.ndim(x) == 2:
                x = np.asarray(x)
            else:
                raise ValueError('Invalid input data shape')
        else:
            raise ValueError('Invalid input data')

        # Determining sizes
        num_channels, sample_len = x.shape

        # Delay validation
        if not np.isscalar(delay):
            delay = np.asarray(delay)
            # Same delay vector for all channels (sample_len)
            if delay.shape == (sample_len, ):
                delay = np.tile(delay, (num_channels, 1))
            # Constant value for each channel (num_channels)
            elif delay.shape == (num_channels, ):
                delay = np.tile(delay[np.newaxis, :].T, sample_len)

            if delay.shape != (num_channels, sample_len):
                raise ValueError('Invalid delay shape. Must be ' +
                                 '(num_channels, sample_len)')
        else:
            delay = np.ones((num_channels, sample_len))*delay

        # Clipping too large delay values
        delay[delay > self.max_delay] = self.max_delay

        # Constructing initials (channels_num, max_delay)

        # Default is zeros
        if self.initials is None:
            self.initials = np.zeros((num_channels, self.max_delay))
        # Constant scalar value
        elif np.isscalar(self.initials):
            self.initials = self.initials * np.ones(
                (num_channels, self.max_delay))
        else:
            self.initials = np.asarray(self.initials)

            # Constant values per channel
            if self.initials.shape == (self.max_delay, ):
                self.initials = np.asarray(self.initials)[np.newaxis, :]
            # Checking consistency
            elif self.initials.shape != (num_channels, self.max_delay):
                raise ValueError('Invalid shape for initial conditions! ' +
                                 'It must be (max_delay, num_channels)')
        return x, delay

    def step(self, x, delay):
        """
        Calculates delay for input signal x.

        Args:
            x (array_like): An input signal with shape
                (num_channels, sample_len). The shape of the signal should
                not change between sequential calls to stop (use reset method
                for this case). Each column is considered as a frame with
                sequential time samples from independent channel.
            delay (float or array_like): For fixed time delay use constant
                value. If the delay is a vector with length num_channles each
                channel will have the corresponding delay. For time-variable
                delay use array with shape (num_channels, sample_len).
        """
        # Input validation
        x, delay = self.step_input_validation(x, delay)
        num_channels, sample_len = x.shape

        # Rounding
        delay = np.fix(delay).astype(np.int)

        # Calculating delay for each channel
        res = np.zeros((num_channels, sample_len))
        space = np.arange(sample_len)
        for i in range(num_channels):
            # Constructing buffer (max_delay + 1, sample_len)
            buffer = construct_roll_buffer(
                x[i], self.initials[i], self.max_delay)
            # Variable integer delay
            res[i] = buffer[delay[i], space]

        # Updating initials
        self.initials = np.hstack((self.initials, x))[:, -self.max_delay:]

        return np.squeeze(res)


class VariableFractionalDelay(VariableIntegerDelay):
    """
    Delays input by time-varying (noninteger) number of sample periods by
    using linear interpolation.

    """
    def step(self, x, delay):
        # Input validation
        x, delay = self.step_input_validation(x, delay)
        num_channels, sample_len = x.shape

        # Calculating delay
        res = np.zeros((num_channels, sample_len), dtype=x.dtype)
        for i in range(num_channels):
            # Constructing buffer (max_delay + 1, sample_len)
            buffer = construct_roll_buffer(
                x[i], self.initials[i], self.max_delay)

            res[i] = self.interpolate(delay[i], buffer)

        # Updating initials
        self.initials = np.hstack((self.initials, x))[:, -self.max_delay:]

        return np.squeeze(res)

    def interpolate(self, delay, buffer):
        space = np.arange(buffer.shape[1])

        # Variable integer delay
        def u(d):
            return buffer[d, space]

        # Integer delay
        di = np.floor(delay).astype(np.int)

        # Fractional delay
        df = delay - di

        # Interpolation
        return (1 - df)*u(di) + df*u(di + 1)
