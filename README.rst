**************
Variable Delay 
**************

This module implements delay input by time-varying number of sample periods.

Variable Integer Delay
----------------------
Delay the input by a time-varying integer number of sample periods.

Description
***********
The VariableIntegerDelay object creates an object with internal memory.

The constructor accepts two parameters - maximum computable delay and initials conditions.
The process of computation is achieved through calling **step** method.
The **step** method takes two parameters - signal channels samples and delay values.

The internal memory is used to remember previous samples to allow sequential calls to the **step** method with updated initial conditions.
To reset the internal buffer call the **reset** method.

The calculation of the variable integer delay is done by constructing special buffer *U* where the delayed signal *y* can be computed via following formula

::

    y = U[delay]; 

The shape of this matrix is **(max_delay + 1, sample_len)**. For initial conditions **[0, 0, 1, 2, 3]** and signal **[4, 5, 6, 7]** and the **max_delay = 5** the buffer will be following:

::

    [[4, 5, 6, 7],
     [3, 4, 5, 6],
     [2, 3, 4, 5],
     [1, 2, 3, 4],
     [0, 1, 2, 3],
     [0, 0, 1, 2]]  


Note, the maximum computable delay is guided by **max_delay** constructor parameter which is defaults to 100.

By passing input signal as **x** and delay values as **delay** to the **step** method one can choose different modes.

Available regimes of processing:
    - *Single channel mode* - in this case signal and delay are 1-d arrays of the same length.
    - *Multiple channel mode* - in this case signal and delay are 2-d arrays with shape **(num_channels, sample_len)**, or you can also use 1-d array for delay to determine a constant delay for each channel.


The initial conditions can be set with multiple ways with **initials** parameter of the constructor.

Controlling the initial conditions:
    - The default initial conditions are zero (**initials=None**).
    - Constant initial conditions (**initials=4.3**).
    - Each channel have constant initials (1-d array with length **num_channels**).
    - Each channel have different initials (2-d array with shape **(num_channels, max_delay)**).

Example
*******

This example delay a single channel signal part by part.

::

    # Creating Variable Integer Delay object
    dobj = VariableIntegerDelay()

    # Generating input signal
    x = np.arange(100)

    # Allocating buffer for output signal
    y = np.zeros(100)

    # Iterating through parts of the signal
    for k in range(10):
        # Generating delay values
        delay = k*np.ones(10);
    
        # Taking a slice
        x_temp = x[k*10: (k + 1)*10]

        # Calculating delay
        y[k*10: (k + 1)*10] = dobj.step(x_temp, delay)

        plt.stem(x)
        plt.stem(y, markerfmt='or', basefmt='r')



Variable Fractional Delay
-------------------------

Delay the input by a time-varying possibly noninteger number of sample periods.

Description
***********
The Variable Fractional Delay object is very simular to the Integer Fractional Delay. The only differs is that these stored buffer is accessed - a fractional delay requires the computation of a value by interpolation from the nearby samples in memory.

::

    di = floor(delay)                       % di - integer delay
    df = delay - di                         % df - fractional delay
    y = (1-df)*U(di) + df*U(di+1)


Example
*******

This example computes variable fractional delay for two harmonic channels.

::

    sample_len = 100

    # Creating samples for two channels
    x = np.array([np.sin(np.arange(sample_len)),
                  np.cos(np.arange(sample_len))])

    # Creating delay values (same for all channels)
    delay = np.linspace(1, 20, num=sample_len)

    # Creating Variable Fractional Delay object
    dobj = VariableFractionalDelay()

    # Computing delay
    y = dobj.step(x, delay)

    # Plot the result
    for i in range(2):
       plt.figure()
       plt.stem(x[i])
       plt.stem(y[i], markerfmt='or', basefmt='r')
